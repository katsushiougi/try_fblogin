define () ->

  class Login extends Backbone.View

    initialize: ->

    ###
    ログイン開始
    ###
    start: ->
      $.ajaxSetup({ cache: true })
      $.getScript "//connect.facebook.net/en_UK/all.js", () =>
        FB.init
          appId : "361996700613480"
        @_login()

    ###
    ログイン処理
    ###
    _login: ->
      FB.login (response) =>
        if response.authResponse
          console.log('Welcome!  Fetching your information.... ');
          FB.api "/me/friends", (response) =>
            console.log response
            # console.log('Good to see you, ' + response.name + '.')
        else
          alert "ログインに失敗しましたー"
      ,scope: 'manage_friendlists,publish_actions,publish_stream'
      return


  ###
  Exports
  ###
  return Login