(function() {
  require(["views/Login"], function(Login) {
    var $btn, login,
      _this = this;
    $btn = $("#login");
    login = new Login();
    return $btn.on("click", function() {
      return login.start();
    });
  });

}).call(this);
