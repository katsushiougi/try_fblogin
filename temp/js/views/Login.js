(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(function() {
    var Login, _ref;
    Login = (function(_super) {
      __extends(Login, _super);

      function Login() {
        _ref = Login.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      Login.prototype.initialize = function() {};

      /*
      ログイン開始
      */


      Login.prototype.start = function() {
        var _this = this;
        $.ajaxSetup({
          cache: true
        });
        return $.getScript("//connect.facebook.net/en_UK/all.js", function() {
          FB.init({
            appId: "361996700613480"
          });
          return _this._login();
        });
      };

      /*
      ログイン処理
      */


      Login.prototype._login = function() {
        var _this = this;
        FB.login(function(response) {
          if (response.authResponse) {
            console.log('Welcome!  Fetching your information.... ');
            return FB.api("/me/friends", function(response) {
              return console.log(response);
            });
          } else {
            return alert("ログインに失敗しましたー");
          }
        }, {
          scope: 'manage_friendlists,publish_actions,publish_stream'
        });
      };

      return Login;

    })(Backbone.View);
    /*
    Exports
    */

    return Login;
  });

}).call(this);
